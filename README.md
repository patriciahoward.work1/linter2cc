# linter2cc

Linter executor proxy whose goal is to provide output for supported linters in Code Climate format, resorting to a conversion if necessary.

## Usage

### Example 1: shellcheck

```shell
# Instead of running the linter directly:
shellcheck -x *

# Prefix it with linter2cc, and the output will be done in Code Climate format
linter2cc shellcheck -x *
```

Output:
```json
[
  {
    "description": "shellcheck: SC2207 - Prefer mapfile or read -a to split command output (or quote to avoid splitting).",
    "location": {
      "path": "linter2cc",
      "lines": {
        "begin": 43,
        "end": 43
      }
    }
  },
  {
    "description": "shellcheck: SC2206 - Quote to prevent word splitting/globbing, or split robustly with mapfile or read -a.",
    "location": {
      "path": "linter2cc",
      "lines": {
        "begin": 47,
        "end": 47
      }
    }
  },
  {
    "description": "shellcheck: SC2148 - Tips depend on target shell and yours is unknown. Add a shebang or a 'shell' directive.",
    "location": {
      "path": "README.md",
      "lines": {
        "begin": 1,
        "end": 1
      }
    }
  },
  {
    "description": "shellcheck: SC2046 - Quote this to prevent word splitting.",
    "location": {
      "path": "README.md",
      "lines": {
        "begin": 9,
        "end": 15
      }
    }
  },
  {
    "description": "shellcheck: SC2006 - Use $(...) notation instead of legacy backticked `...`.",
    "location": {
      "path": "README.md",
      "lines": {
        "begin": 9,
        "end": 15
      }
    }
  },
  {
    "description": "shellcheck: SC2154 - files is referenced but not assigned.",
    "location": {
      "path": "README.md",
      "lines": {
        "begin": 11,
        "end": 11
      }
    }
  },
  {
    "description": "shellcheck: SC2086 - Double quote to prevent globbing and word splitting.",
    "location": {
      "path": "README.md",
      "lines": {
        "begin": 11,
        "end": 11
      }
    }
  },
  {
    "description": "shellcheck: SC2086 - Double quote to prevent globbing and word splitting.",
    "location": {
      "path": "README.md",
      "lines": {
        "begin": 14,
        "end": 14
      }
    }
  },
  {
    "description": "shellcheck: SC2046 - Quote this to prevent word splitting.",
    "location": {
      "path": "README.md",
      "lines": {
        "begin": 19,
        "end": 25
      }
    }
  },
  {
    "description": "shellcheck: SC2006 - Use $(...) notation instead of legacy backticked `...`.",
    "location": {
      "path": "README.md",
      "lines": {
        "begin": 19,
        "end": 25
      }
    }
  }
]
```

### Example 2: golangci-lint

```shell
# Instead of running the linter directly:
golangci-lint ./...

# Prefix it with linter2cc, and the output will be done in Code Climate format
linter2cc golangci-lint ./...
```
